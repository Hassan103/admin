import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorClinics = () => {

    const [ doctorSpecializationData , setDoctorSpecializationData] =  useState([
        {
            no : 1,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Active',
            actions : ''
        },
        {
            no : 2,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Active',
            actions : ''
        },
        {
            no : 3,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Inactive',
            actions : ''
        },
        {
            no : 1,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Active',
            actions : ''
        },
        {
            no : 2,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Active',
            actions : ''
        },
        {
            no : 3,
            doc_clinic_name : 'Hila-e-ahmar Hospital',
            doc_clinic_phone : '+923152321353',
            doc_clinic_doctor : 'Tariq Mustafa Memon',    
            doc_clinic_city : 'Karachi',
            status : 'Active',
            actions : ''
        },   
    ]); 
    const  doctorClinicsColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doc_clinic_name',
        },
        {
            Header : 'Phone',
            accessor : 'doc_clinic_phone',
        }, 
        {
            Header : 'Doctor',
            accessor : 'doc_clinic_doctor',
        },  
        {
            Header : 'Status',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <span className={ props.status == 'Active' ? 'active px-4 py-2' : 'inactive px-4 py-2' }> { props.status }</span>
                )
            },
        },
        {
            Header : 'Actions',
            accessor : 'actions',
            Cell : (props) => {
                return( 
                    <div className="actions_btn">   
                        <a href="https://admin.doclink.health/doctors/payout/187" 
                            class="common-icon-btn blue-icon-btn" 
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Payout">
                            <svg xmlns="http://www.w3.org/2000/svg"   version="1.1" x="0px" y="0px" width="15px" height="15px" viewBox="0 0 12 12" fill="#ffffff">
                                <path class="st0" d="M6,12c-3.31,0-6-2.69-6-6s2.69-6,6-6s6,2.69,6,6S9.31,12,6,12z M6,1.29C3.4,1.29,1.29,3.4,1.29,6  S3.4,10.71,6,10.71S10.71,8.6,10.71,6S8.6,1.29,6,1.29z"/>
                                <path class="st0" d="M6,11.25c2.9,0,5.25-2.35,5.25-5.25S8.9,0.75,6,0.75S0.75,3.1,0.75,6S3.1,11.25,6,11.25z M12,6  c0,3.31-2.69,6-6,6S0,9.31,0,6s2.69-6,6-6S12,2.69,12,6z"/>
                                <path class="st0" d="M8.14,8.78c-0.11,0-0.21-0.03-0.31-0.08L5.32,7.27C5.12,7.16,5,6.94,5,6.71V2.79c0-0.35,0.29-0.64,0.64-0.64  c0.35,0,0.64,0.29,0.64,0.64v3.56l2.18,1.24c0.31,0.19,0.41,0.58,0.23,0.89C8.57,8.66,8.36,8.78,8.14,8.78z"/>
                                <path class="st0" d="M5.63,2.25C5.83,2.25,6,2.42,6,2.63v3.91l2.44,1.39c0.18,0.11,0.23,0.34,0.13,0.51  c-0.1,0.17-0.32,0.23-0.5,0.14l-2.63-1.5C5.32,7.01,5.25,6.88,5.25,6.75V2.63C5.25,2.42,5.42,2.25,5.63,2.25z"/>
                            </svg>
                        </a>
                         <a href="https://admin.doclink.health/doctors/ratings/187" 
                            class="common-icon-btn blue-icon-btn"
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Feedback">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff"><path class="cls-1" d="M1360.35,272.539l-0.02-.017a2.01,2.01,0,0,0-2.83.127l-7.15,7.8a0.629,0.629,0,0,0-.15.246l-0.84,2.512a0.942,0.942,0,0,0,.13.859,0.963,0.963,0,0,0,.78.4h0a0.927,0.927,0,0,0,.38-0.081l2.44-1.061a0.624,0.624,0,0,0,.23-0.166l7.16-7.8A2,2,0,0,0,1360.35,272.539Zm-9.38,10.2,0.49-1.474,0.05-.045,0.93,0.851-0.04.045Zm8.49-8.315-6.09,6.631-0.93-.851,6.08-6.631a0.614,0.614,0,0,1,.46-0.2,0.641,0.641,0,0,1,.42.162l0.02,0.017A0.62,0.62,0,0,1,1359.46,274.428Zm-0.5,3.919a0.689,0.689,0,0,0-.69.687v5.841a1.757,1.757,0,0,1-1.76,1.749h-8.37a1.757,1.757,0,0,1-1.76-1.749v-8.27a1.756,1.756,0,0,1,1.76-1.749h6.06a0.688,0.688,0,1,0,0-1.376h-6.06a3.137,3.137,0,0,0-3.14,3.125v8.27a3.137,3.137,0,0,0,3.14,3.125h8.37a3.137,3.137,0,0,0,3.14-3.125v-5.841A0.689,0.689,0,0,0,1358.96,278.347Z" transform="translate(-1345 -272)"></path></svg>
                        </a>  
                    </div> 
                )
            }
        }
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Clinics</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorSpecializationData }
                                            columns={ doctorClinicsColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorClinics;

 