import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorSalesReferal = () => {

    const [ doctorSalesReferralData , setDoctorSalesReferralData] =  useState([
        {
            no : 1,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Active',
            actions : ''
        },
        {
            no : 2,
            doc_sales_ref_name : 'Govind Kumar',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Inactive',
            actions : ''
        },
        {
            no : 3,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Active',
            actions : ''
        },
        {
            no : 4,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Active',
            actions : ''
        },
        {
            no : 5,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Inactive',
            actions : ''
        },
        {
            no : 6,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Active',
            actions : ''
        },
        {
            no : 7,
            doc_sales_ref_name : 'Sameera',
            doc_sales_ref_clinic : 'Hassan Clinic',
            doc_sales_ref_doctor : 'Mohsin Ali Mustafa',    
            doc_sales_ref_phone : '+923332321232',
            doc_sales_ref_id : 'same324',
            doc_sales_ref_patient : '0',
            doc_sales_ref_last_date : 'N/A',
            status : 'Inactive',
            actions : ''
        },



    ]); 
    const [isShow, setIsShow] =  useState(false);



    // Is Show Referral ID

    const isShowPassword = (e) => {
        setIsShow(!isShow);
    }

    const  doctorClinicsColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doc_sales_ref_name',
        },
        {
            Header : 'Clinic',
            accessor : 'doc_sales_ref_clinic',
        }, 
        {
            Header : 'Doctor',
            accessor : 'doc_sales_ref_doctor',
        },  
        {
            Header : 'Phone',
            accessor : 'doc_sales_ref_phone',
        },  
        {
            Header : 'Referral ID',
            accessor : 'doc_sales_ref_id',
            Cell : (props) => {
                return(
                    <div className="ref-id d-flex">
                        <input type={ isShow  ? 'text' : 'password'  } value={ props.doc_sales_ref_id } />
                        <div className="actions_btn">
                            <a href="javascript:void(0)" class="common-icon-btn blue-icon-btn" onClick = { isShowPassword }>
                                <svg xmlns="http://www.w3.org/2000/svg" class="svg-btn" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff"><g>
                                    <path d="M14.1,7.1c0.5,0.5,0.5,1.2,0,1.7c-1.1,1.2-3.5,3.2-6.1,3.2s-5-2-6.1-3.2c-0.5-0.5-0.5-1.2,0-1.7 C3,5.9,5.3,3.9,8,3.9S13,5.9,14.1,7.1z"/>
                                    <circle fill="#299cce" cx="8" cy="8" r="2.8"/> </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                )
            }
        }, 
        {
            Header : 'Referral Patients',
            accessor : 'doc_sales_ref_patient',
        }, 
        {
            Header : 'Last Referral Date',
            accessor : 'doc_sales_ref_last_date',
        }, 
        
        {
            Header : 'Status',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <span className={ props.status == 'Active' ? 'active px-4 py-2' : 'inactive px-4 py-2' }> { props.status }</span>
                )
            },
        },
        {
            Header : 'Actions',
            accessor : 'actions',
            Cell : (props) => {
                return( 
                    <div className="actions_btn">   
                        <a href="https://admin.doclink.health/doctors/payout/187" 
                            class="common-icon-btn blue-icon-btn" 
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Payout">
                            <svg xmlns="http://www.w3.org/2000/svg" class="svg-btn" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff"><g>
                                <path d="M14.1,7.1c0.5,0.5,0.5,1.2,0,1.7c-1.1,1.2-3.5,3.2-6.1,3.2s-5-2-6.1-3.2c-0.5-0.5-0.5-1.2,0-1.7 C3,5.9,5.3,3.9,8,3.9S13,5.9,14.1,7.1z"/>
                                <circle fill="#299cce" cx="8" cy="8" r="2.8"/> </g>
                            </svg>
                        </a>
                         <a href="https://admin.doclink.health/doctors/ratings/187" 
                            class="common-icon-btn blue-icon-btn"
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Feedback">
                           <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff"><path class="cls-1" d="M1360.35,272.539l-0.02-.017a2.01,2.01,0,0,0-2.83.127l-7.15,7.8a0.629,0.629,0,0,0-.15.246l-0.84,2.512a0.942,0.942,0,0,0,.13.859,0.963,0.963,0,0,0,.78.4h0a0.927,0.927,0,0,0,.38-0.081l2.44-1.061a0.624,0.624,0,0,0,.23-0.166l7.16-7.8A2,2,0,0,0,1360.35,272.539Zm-9.38,10.2,0.49-1.474,0.05-.045,0.93,0.851-0.04.045Zm8.49-8.315-6.09,6.631-0.93-.851,6.08-6.631a0.614,0.614,0,0,1,.46-0.2,0.641,0.641,0,0,1,.42.162l0.02,0.017A0.62,0.62,0,0,1,1359.46,274.428Zm-0.5,3.919a0.689,0.689,0,0,0-.69.687v5.841a1.757,1.757,0,0,1-1.76,1.749h-8.37a1.757,1.757,0,0,1-1.76-1.749v-8.27a1.756,1.756,0,0,1,1.76-1.749h6.06a0.688,0.688,0,1,0,0-1.376h-6.06a3.137,3.137,0,0,0-3.14,3.125v8.27a3.137,3.137,0,0,0,3.14,3.125h8.37a3.137,3.137,0,0,0,3.14-3.125v-5.841A0.689,0.689,0,0,0,1358.96,278.347Z" transform="translate(-1345 -272)"></path></svg>
                        </a>  
                    </div> 
                )
            }
        }
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Sales Referrals</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorSalesReferralData }
                                            columns={ doctorClinicsColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorSalesReferal;

 
 