import React, { useState } from 'react';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

function ViewDoctorModal(props) {

    return (
        <Modal isOpen={props.show} toggle={props.toggle}>
            <ModalBody>
                <h4 className="modal-title modal-heading" id="exampleModalLabel">Doctor Details</h4>
                <div class="table-responsive">
                    <table class="table table-striped">

                        <tbody>
                            <tr>
                                <th>Name</th>
                                <td>Noman</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>+923330315526</td>
                            </tr>


                            <tr>
                                <th>Email</th>
                                <td>farhanuddinniazi@gmail.com</td>
                            </tr>

                            <tr>
                                <th>PMDC No.</th>
                                <td>086546893</td>
                            </tr>

                            <tr>
                                <th>CNIC No.</th>
                                <td></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </ModalBody>
        </Modal>
    );
}

export default ViewDoctorModal;