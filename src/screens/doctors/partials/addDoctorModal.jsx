import React, { useState } from 'react';
import Select from 'react-select';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const customStyles = {
    container: (base) => ({
        ...base,
        width: '100%',
        maxWidth: '230px',
        border: '1px solid #299cce',
        borderRadius: '5px',
        color: '#299cce',
    }),
    control: (base) => ({
        ...base,
        border: 0,
        outline: 0,
        borderRadius: '5px',
    }),
    indicatorSeparator: (base) => ({
        ...base,
        display: 'none'
    }),
    dropdownIndicator: base => ({
        ...base,
        display: 'none'        
    }),
    singleValue: base => ({
        ...base,
        color: '#299cce'
    })
}

function AddDoctorModal(props) {

    const [specializations, setSpecializations] = useState([
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' },
    ]);

    const [selectedSpecialization, setSelectedSpecialization] = useState();

    const [photo, setPhoto] = useState('');

    let handleSpecialization = selectedOption => {
        setSelectedSpecialization(selectedOption);
    };

    let handlePhotoChange = (e) => {
        setPhoto(e.target.files[0]);
    }

    let handleSubmit = () => {
        
    }

    return (
        <Modal isOpen={props.show} toggle={props.toggle}>
            <ModalBody>
                <h4 className="modal-title modal-heading" id="exampleModalLabel">Add Doctor</h4>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>Name</label>
                                    <input class="form-control" name="name" type="text" />
                                    <span id="error-name" class="error error-name invalid-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>Mobile Number</label>
                                    <input class="form-control" name="phone" type="tel" value="" />
                                    <span class="error invalid-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>Specializations</label>
                                    <Select
                                        styles={customStyles}
                                        value={selectedSpecialization}
                                        onChange={handleSpecialization}
                                        options={specializations}
                                        placeholder=""
                                    />
                                    <span class="error invalid-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>PMDC No.</label>
                                    <input class="form-control" name="pmdc_no" type="text" value="" />
                                    <span class="error invalid-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>Email</label>
                                    <input class="form-control" name="email" type="text" value="" />
                                    <span class="error invalid-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group modal-form-group">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <label>Profile Photo</label>
                                    <input class="form-control" name="photo" type="file" id="photo" onChange={handlePhotoChange} />
                                    <span class="error invalid-feedback"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-end">
                        <button class="btn btn-rounded btn-cancel mr-2" type="button" data-dismiss="modal" aria-label="Close" onClick={props.toggle}>
                            Cancel
                        </button>
                        <input class="btn btn-primary btn-rounded" type="submit" value="Save" onClick={handleSubmit}/>
                    </div>

                </form>
            </ModalBody>
        </Modal>
    );
}

export default AddDoctorModal;