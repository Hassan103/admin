import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorPackeges = () => {

    const [ doctorPackgesData , setDoctorPackgesData] =  useState([
        {
            no : 1,
            doc_packege_name : 'Pay monthly',
            doc_packege_duration : '30',            	
            doc_packege_percentage : '100',
            status : 'Active',
            actions : ''
        },
         
    ]);

 
 
    const  doctorPackegesColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doc_packege_name',
        },
        {
            Header : 'Duration',
            accessor : 'doc_packege_duration',
        },
        {
            Header : 'Percentage',
            accessor : 'doc_packege_percentage',
        }, 
        {
            Header : 'Status',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <span className="active px-4 py-2"> { props.status }</span>
                )
            },
        },
        {
            Header : 'Actions',
            accessor : 'actions',
            Cell : (props) => {
                return( 
                    <div className="actions_btn">   
                        <a href="https://admin.doclink.health/doctors/payout/187" 
                            class="common-icon-btn blue-icon-btn" 
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Payout">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff">
                                <path class="cls-1" d="M1360.35,272.539l-0.02-.017a2.01,2.01,0,0,0-2.83.127l-7.15,7.8a0.629,0.629,0,0,0-.15.246l-0.84,2.512a0.942,0.942,0,0,0,.13.859,0.963,0.963,0,0,0,.78.4h0a0.927,0.927,0,0,0,.38-0.081l2.44-1.061a0.624,0.624,0,0,0,.23-0.166l7.16-7.8A2,2,0,0,0,1360.35,272.539Zm-9.38,10.2,0.49-1.474,0.05-.045,0.93,0.851-0.04.045Zm8.49-8.315-6.09,6.631-0.93-.851,6.08-6.631a0.614,0.614,0,0,1,.46-0.2,0.641,0.641,0,0,1,.42.162l0.02,0.017A0.62,0.62,0,0,1,1359.46,274.428Zm-0.5,3.919a0.689,0.689,0,0,0-.69.687v5.841a1.757,1.757,0,0,1-1.76,1.749h-8.37a1.757,1.757,0,0,1-1.76-1.749v-8.27a1.756,1.756,0,0,1,1.76-1.749h6.06a0.688,0.688,0,1,0,0-1.376h-6.06a3.137,3.137,0,0,0-3.14,3.125v8.27a3.137,3.137,0,0,0,3.14,3.125h8.37a3.137,3.137,0,0,0,3.14-3.125v-5.841A0.689,0.689,0,0,0,1358.96,278.347Z" transform="translate(-1345 -272)"/>
                            </svg>
                        </a>
                        {/* <a href="https://admin.doclink.health/doctors/ratings/187" 
                            class="common-icon-btn blue-icon-btn"
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Feedback">
                            <svg width="15" height="15" viewBox="0 0 22 22" fill="white">
                                <path d="M9 22C8.4 22 8 21.6 8 21V18H4C2.9 18 2 17.1 2 16V4C2 2.9 2.9 2 4 2H20C21.1 2 22 2.9 22 4V16C22 17.1 21.1 18 20 18H13.9L10.2 21.7C10 21.9 9.8 22 9.5 22H9M10 16V19.1L13.1 16H20V4H4V16H10M16.3 6L14.9 9H17V13H13V8.8L14.3 6H16.3M10.3 6L8.9 9H11V13H7V8.8L8.3 6H10.3Z"></path>
                            </svg>
                        </a> */}
                        <a href="javascript:void(0);" class="common-icon-btn blue-icon-btn"  title="" data-tooltip="tooltip" data-original-title="Delete Doctor">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 13 16" fill="#ffffff">
                                <path class="cls-1" d="M1393.75,274H1391v-0.5a1.5,1.5,0,0,0-1.5-1.5h-2a1.5,1.5,0,0,0-1.5,1.5V274h-2.75a1.251,1.251,0,0,0-1.25,1.25V277a0.5,0.5,0,0,0,.5.5h0.27l0.44,9.071A1.487,1.487,0,0,0,1384.7,288h7.6a1.487,1.487,0,0,0,1.49-1.429l0.44-9.071h0.27a0.5,0.5,0,0,0,.5-0.5v-1.75a1.251,1.251,0,0,0-1.25-1.25h0Zm-6.75-.5a0.5,0.5,0,0,1,.5-0.5h2a0.5,0.5,0,0,1,.5.5V274h-3v-0.5Zm-4,1.75a0.249,0.249,0,0,1,.25-0.25h10.5a0.249,0.249,0,0,1,.25.25v1.25h-11v-1.25Zm9.8,11.274a0.51,0.51,0,0,1-.5.476h-7.6a0.51,0.51,0,0,1-.5-0.476l-0.43-9.024h9.46Zm-4.3-.524a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1388.5,286Zm2.5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1391,286Zm-5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1386,286Z" transform="translate(-1382 -272)"/>
                            </svg>
                        </a> 
                    </div> 
                )
            }
        }
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Packges</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorPackgesData }
                                            columns={ doctorPackegesColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorPackeges;








  