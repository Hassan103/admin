import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorRejectRequest = () => {

    const [ doctorRejectRequestData , setDoctorRejectRequestData] =  useState([
        {
            no : 1,
            doc_reject_name : 'Dr Mohsin',
            doc_reject_email : 'mohsin@gmail.com',            	
            doc_reject_phone : '+925555555555',
            doc_reject_reason : 'Testing user', 
        },
        {
            no : 2,
            doc_reject_name : 'Dr Mohsin',
            doc_reject_email : 'mohsin@gmail.com',            	
            doc_reject_phone : '+925555555555',
            doc_reject_reason : 'Testing user', 
        },
        {
            no : 3,
            doc_reject_name : 'Dr Mohsin',
            doc_reject_email : 'mohsin@gmail.com',            	
            doc_reject_phone : '+925555555555',
            doc_reject_reason : 'Testing user', 
        },
        {
            no : 4,
            doc_reject_name : 'Dr Mohsin',
            doc_reject_email : 'mohsin@gmail.com',            	
            doc_reject_phone : '+925555555555',
            doc_reject_reason : 'Testing user', 
        },
         
    ]);

 
 
    const  doctorRequestColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doc_reject_name',
        },
        {
            Header : 'Email',
            accessor : 'doc_reject_email',
        },
        {
            Header : 'Phone',
            accessor : 'doc_reject_phone',
        }, 
        {
            Header : 'Reason',
            accessor : 'doc_reject_reason',
        },  
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Doctor Rejected</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorRejectRequestData }
                                            columns={ doctorRequestColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorRejectRequest;





 