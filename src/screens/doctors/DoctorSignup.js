import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorSignup = () => {

    const [ doctorSignupData , setDoctorSignupData] =  useState([
        {
            no : 1,
            doc_signup_name : 'Dr.Nadeem Asghar',
            doc_speciality : 'General physician, M.B,B.S Dow',            	
            doc_signup_email : 'drnadeemasghar@gmail.com',
            doc_signup_phone : '+923322242152',
            doc_signup_city : '+923322242152'
        },
        {
            no : 2,
            doc_signup_name : 'Dr.Aimen',
            doc_speciality : 'General physician, M.B,B.S Dow',            	
            doc_signup_email : 'drnadeemasghar@gmail.com',
            doc_signup_phone : '+923322242152',
            doc_signup_city : '+923322242152'
        },
        {
            no : 3,
            doc_signup_name : 'Dr.Ammar Itrat',
            doc_speciality : 'General physician, M.B,B.S Dow',            	
            doc_signup_email : 'drnadeemasghar@gmail.com',
            doc_signup_phone : '+923322242152',
            doc_signup_city : '+923322242152'
        },
        {
            no : 4,
            doc_signup_name : 'Dr.Ammar Itrat',
            doc_speciality : 'General physician, M.B,B.S Dow',            	
            doc_signup_email : 'drnadeemasghar@gmail.com',
            doc_signup_phone : '+923322242152',
            doc_signup_city : '+923322242152'
        },
        {
            no : 5,
            doc_signup_name : 'Dr.Ammar Itrat',
            doc_speciality : 'General physician, M.B,B.S Dow',            	
            doc_signup_email : 'drnadeemasghar@gmail.com',
            doc_signup_phone : '+923322242152',
            doc_signup_city : '+923322242152'
        },

    ]);

 
 
    const  doctorSignupColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doc_signup_name',
        },
        {
            Header : 'Speciality',
            accessor : 'doc_speciality',
        },
        {
            Header : 'Email',
            accessor : 'doc_signup_email',
        }, 
        {
            Header : 'Phone',
            accessor : 'doc_signup_phone',
        }, 
        {
            Header : 'City',
            accessor : 'doc_signup_city',
        },  
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Invitations</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorSignupData }
                                            columns={ doctorSignupColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorSignup;








 
