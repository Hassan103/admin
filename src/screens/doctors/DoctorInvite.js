import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const DoctorInvite = () => {

    const [ doctorInvitationData , setDoctorInvitaionData] =  useState([
        {
            no : 1,
            doc_invitee_name : 'Arden Huff',
            doc_invitee_clinic : 'Laborum Quod dolore',            	
            doc_invitee_phone : '+927595765764',
            doc_invitee_details : ''
        },
        {
            no : 2,
            doc_invitee_name : 'Arden Huff',
            doc_invitee_clinic : 'Laborum Quod dolore',            	
            doc_invitee_phone : '+927595765764',
            doc_invitee_details : ''
        },
        {
            no : 3,
            doc_invitee_name : 'Arden Huff',
            doc_invitee_clinic : 'Laborum Quod dolore',            	
            doc_invitee_phone : '+927595765764',
            doc_invitee_details : ''
        },
        {
            no : 4,
            doc_invitee_name : 'Arden Huff',
            doc_invitee_clinic : 'Laborum Quod dolore',            	
            doc_invitee_phone : '+927595765764',
            doc_invitee_details : ''
        },
        

    ]);

 
 
    const  doctorInvitationColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Invitee Name',
            accessor : 'doc_invitee_name',
        },
        {
            Header : 'Invitee Clinic',
            accessor : 'doc_invitee_clinic',
        },
        {
            Header : 'Invitee Phone',
            accessor : 'doc_invitee_phone',
        }, 
        {
            Header : 'Actions',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <div className="action_btns">
                        <a href="javascript:void(0);" id="showpopup" data-toggle="modal" data-target="#view-modal" data-url="https://admin.doclink.health/doctors/request/show/1220" title="View Details">
                            <img src="https://admin.doclink.health/images/icons/view.svg" alt="Paas `img` Param in `modal_button()` function!" />
                        </a> 
                    </div>
                )
            },
        },
    ]

 
 

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Invitations</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ doctorInvitationData }
                                            columns={ doctorInvitationColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default DoctorInvite;








 