import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';

import DoctorDetails from './partials/viewDoctorModal'; 
import ViewDoctor from './partials/viewDoctorModal';

const VerificationRequest = () => {

    const [showViewDoctorModal, setShowDoctorModal] = useState(false);

    const [ verificationRequestData , setVerificationRequest] =  useState([
        {
            no : 1,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        },
        {
            no : 2,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        },
        {
            no : 3,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        },
        {
            no : 4,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        },
        {
            no : 5,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        },
        {
            no : 6,
            ver_request_name : 'Altaf Hussain',
            ver_request_email : 'Dejavu.shahrukh@gmail.com',            	
            ver_request_phone : '+927595765764',
            actions : ''
        }

    ]);



    const verificationRequestColumn = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Doctor Name',
            accessor : 'ver_request_name',
        },
        {
            Header : 'Email',
            accessor : 'ver_request_email',
        },
        {
            Header : 'Phone',
            accessor : 'ver_request_phone',
        }, 
        {
            Header : 'Actions',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <div className="action_btns">
                        <a href="javascript:void(0);" id="showpopup" data-toggle="modal" data-target="#view-modal" data-url="https://admin.doclink.health/doctors/request/show/1220" title="View Details">
                            <img src="https://admin.doclink.health/images/icons/view.svg" alt="Paas `img` Param in `modal_button()` function!" />
                        </a>
                        <a href="javascript:;" title="Accept request" class="common-btn accept green-btn">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            Approve
                        </a>
                        <a href="javascript:;" title="Reject request" class="red-btn common-btn sweet-reject reject">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        Reject
                    </a>
                    </div>
                )
            },
        },
    ]

 
    let toggleViewDoctorModal = () => {
        setShowDoctorModal(!showViewDoctorModal);
    }

    return(
        <Fragment>
            <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />

                <ViewDoctor show={showViewDoctorModal} toggle={toggleViewDoctorModal} />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Verification Requests</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={verificationRequestData}
                                            columns={verificationRequestColumn}
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </Fragment>
    )
}

export default VerificationRequest;

