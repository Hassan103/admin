import React, { Fragment, useState } from 'react';

import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';
import AddDoctor from './partials/addDoctorModal'; 


const DoctorProfile = () => {

    const [showAddDoctorModal, setShowDoctorModal] = useState(false);
    const [search, setSearch] = useState('');
    const [doctorProfileData, setProfileData ] =  useState([
        {
            no : 1,
            doctor_name : 'Sameera',
            doctor_email : 'sameera@gmail.com',
            doctor_phone : '923241260800' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },
        {
            no : 2,
            doctor_name : 'Zamran Khan',
            doctor_email : 'zamran_2000@hotmail.co.uk',
            doctor_phone : '+923002115681' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },
        {
            no : 3,
            doctor_name : 'Waris Ahmed',
            doctor_email : 'waris@gmail.com',
            doctor_phone : '+923029742324' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },
        {
            no : 4,
            doctor_name : 'Arsalan Ahmed',
            doctor_email : 'waris@gmail.com',
            doctor_phone : '+923029742324' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },
        {
            no : 5,
            doctor_name : 'Jameel Ahmed',
            doctor_email : 'waris@gmail.com',
            doctor_phone : '+923029742324' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },
        {
            no : 6,
            doctor_name : 'Waris Ahmed',
            doctor_email : 'waris@gmail.com',
            doctor_phone : '+923029742324' ,
            app_version : 'v1.7.2',
            status : 'active',
            actions : ''
        },



    ]);

    // Doctor Profile Columns
    const doctorProfileColumns = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Name',
            accessor : 'doctor_name',
        },
        {
            Header : 'Email',
            accessor : 'doctor_email',
        },
        {
            Header : 'Phone',
            accessor : 'doctor_phone',
        },
        {
            Header : 'App Version',
            accessor : 'app_version'
        },
        {
            Header : 'Status',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <span className="active px-4 py-2"> { props.status }</span>
                )
            },
        },
        {
            Header : 'Actions',
            accessor : 'actions',
            Cell : (props) => {
                return( 
                    <div className="actions_btn">   
                        <a href="https://admin.doclink.health/doctors/payout/187" 
                            class="common-icon-btn blue-icon-btn" 
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Payout">
                            <svg width="15" height="15" viewBox="0 0 24 24" fill="white">
                                <path d="M16 11.5C16 10.12 17.12 9 18.5 9S21 10.12 21 11.5 19.88 14 18.5 14 16 12.88 16 11.5M13 3V20H24V3H13M22 16C20.9 16 20 16.9 20 18H17C17 16.9 16.11 16 15 16V7C16.11 7 17 6.11 17 5H20C20 6.11 20.9 7 22 7V16M7 6C8.1 6 9 6.9 9 8S8.1 10 7 10 5 9.1 5 8 5.9 6 7 6M7 4C4.79 4 3 5.79 3 8S4.79 12 7 12 11 10.21 11 8 9.21 4 7 4M7 14C3.13 14 0 15.79 0 18V20H11V18H2C2 17.42 3.75 16 7 16C8.83 16 10.17 16.45 11 16.95V14.72C9.87 14.27 8.5 14 7 14Z"></path>
                            </svg>
                        </a>
                        <a href="https://admin.doclink.health/doctors/ratings/187" 
                            class="common-icon-btn blue-icon-btn"
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Feedback">
                            <svg width="15" height="15" viewBox="0 0 22 22" fill="white">
                                <path d="M9 22C8.4 22 8 21.6 8 21V18H4C2.9 18 2 17.1 2 16V4C2 2.9 2.9 2 4 2H20C21.1 2 22 2.9 22 4V16C22 17.1 21.1 18 20 18H13.9L10.2 21.7C10 21.9 9.8 22 9.5 22H9M10 16V19.1L13.1 16H20V4H4V16H10M16.3 6L14.9 9H17V13H13V8.8L14.3 6H16.3M10.3 6L8.9 9H11V13H7V8.8L8.3 6H10.3Z"></path>
                            </svg>
                        </a>
                        <a href="javascript:void(0);" class="common-icon-btn blue-icon-btn"  title="" data-tooltip="tooltip" data-original-title="Delete Doctor">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 13 16" fill="#ffffff">
                                <path class="cls-1" d="M1393.75,274H1391v-0.5a1.5,1.5,0,0,0-1.5-1.5h-2a1.5,1.5,0,0,0-1.5,1.5V274h-2.75a1.251,1.251,0,0,0-1.25,1.25V277a0.5,0.5,0,0,0,.5.5h0.27l0.44,9.071A1.487,1.487,0,0,0,1384.7,288h7.6a1.487,1.487,0,0,0,1.49-1.429l0.44-9.071h0.27a0.5,0.5,0,0,0,.5-0.5v-1.75a1.251,1.251,0,0,0-1.25-1.25h0Zm-6.75-.5a0.5,0.5,0,0,1,.5-0.5h2a0.5,0.5,0,0,1,.5.5V274h-3v-0.5Zm-4,1.75a0.249,0.249,0,0,1,.25-0.25h10.5a0.249,0.249,0,0,1,.25.25v1.25h-11v-1.25Zm9.8,11.274a0.51,0.51,0,0,1-.5.476h-7.6a0.51,0.51,0,0,1-.5-0.476l-0.43-9.024h9.46Zm-4.3-.524a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1388.5,286Zm2.5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1391,286Zm-5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1386,286Z" transform="translate(-1382 -272)"/>
                            </svg>
                        </a> 
                    </div> 
                )
            }
        }


    ];
    // End Doctor Profile Columns
 



    let toggleAddDoctorModal = () => {
        setShowDoctorModal(!showAddDoctorModal);
    }

    return (
        <div className="dashboard-main-wrapper">
            <Header />
            <Sidebar />

            <AddDoctor show={showAddDoctorModal} toggle={toggleAddDoctorModal} />
            <div className="dashboard-wrapper">
                <div className="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="card-header">
                                        Doctors
                                        <button class="btn btn-primary btn-sm btn-icons btn-rounded float-right" title="New Doctor" onClick={toggleAddDoctorModal}>
                                            <img src="https://admin.doclink.health/images/icons/add.svg" alt="add.svg" class="mr-1" />New Doctor
                                        </button>
                                    </h2>

                                    <div class="search search-filter">
                                        <form>
                                            <input type="text" placeholder="Search by name, email, PMDC number or phone" name="search" value={search} class="form-control ml-2" onChange={e => setSearch(e.target.value)} />
                                        </form>
                                    </div>
                                    {/* Doctor Profile Tables */}
                                    <div class="table-responsive table-new">
                                    <Table
                                        data={doctorProfileData}
                                        columns={doctorProfileColumns}
                                    /> 
                                    </div>
                                    {/* End Doctor Profile Tables */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}



export default DoctorProfile;