import React, { Fragment, useState } from 'react';
import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';




const PatientProfile = () => {

    const [ patientProfileData , setpatientProfileData] =  useState([
     {
        no : '1',
        patient_mrn : '3141',
        patient_name : 'Syed Farhan Shah',
        patient_email : 'syed.farhan@gmail.com',
        patient_phone : '+92208641239',
        patient_connected_doctor : 'N/A',
        patient_ref_code : 'N/A',
        patient_session : '0',
        patient_date : new Date(),
        patient_app_version : 'v1.7.2',
        status : 'Active',
        actions : ''
     },
     {
        no : '2',
        patient_mrn : '3141',
        patient_name : 'Syed Farhan Shah',
        patient_email : 'syed.farhan@gmail.com',
        patient_phone : '+92208641239',
        patient_connected_doctor : 'N/A',
        patient_ref_code : 'N/A',
        patient_session : '0',
        patient_date : new Date(),
        patient_app_version : 'v1.7.2',
        status : 'Active',
        actions : ''
     },
     {
        no : '3',
        patient_mrn : '3141',
        patient_name : 'Syed Farhan Shah',
        patient_email : 'syed.farhan@gmail.com',
        patient_phone : '+92208641239',
        patient_connected_doctor : 'N/A',
        patient_ref_code : 'N/A',
        patient_session : '0',
        patient_date : new Date(),
        patient_app_version : 'v1.7.2',
        status : 'Inactive',
        actions : ''
     },
     {
        no : '4',
        patient_mrn : '3141',
        patient_name : 'Syed Farhan Shah',
        patient_email : 'syed.farhan@gmail.com',
        patient_phone : '+92208641239',
        patient_connected_doctor : 'N/A',
        patient_ref_code : 'N/A',
        patient_session : '0',
        patient_date : new Date(),
        patient_app_version : 'v1.7.2',
        status : 'Active',
        actions : ''
     }

          
        
    ]); 
    const  patientProfileColumn  = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'MRN',
            accessor : 'patient_mrn',
        },
        {
            Header : 'Name',
            accessor : 'patient_name',
        }, 
        {
            Header : 'Email',
            accessor : 'patient_email',
        }, 
        {
            Header : 'Phone',
            accessor : 'patient_phone',
        },  
        {
            Header : 'Connected Doctor',
            accessor : 'patient_connected_doctor',
        },
        {
            Header : 'Ref Code',
            accessor : 'patient_ref_code',

        },
        {
            Header : 'Sessions',
            accessor : 'patient_session',
        },  
        {
            Header : 'Joining Date',
            accessor : 'patient_date',
            Cell : (props) => {
                return(
                    <>
                    <span> {   props.patient_date.toLocaleDateString()   }</span>
   
                    </>
                )
            }
        },
        {
            Header : 'App Version',
            accessor : 'patient_app_version',
        },      
        {
            Header : 'Status',
            accessor : 'status',
            Cell: (props) => {
                return (
                    <span className={ props.status == 'Active' ? 'active px-4 py-2' : 'inactive px-4 py-2' }> { props.status }</span>
                )
            },
        },
        {
            Header : 'Actions',
            accessor : 'actions',
            Cell : (props) => {
                return( 
                    <div className="actions_btn">   
                         <a href="https://admin.doclink.health/doctors/ratings/187" 
                            class="common-icon-btn blue-icon-btn"
                            title="" 
                            data-tooltip="tooltip" 
                            data-original-title="Doctor Feedback">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 16 16" fill="#ffffff"><path class="cls-1" d="M1360.35,272.539l-0.02-.017a2.01,2.01,0,0,0-2.83.127l-7.15,7.8a0.629,0.629,0,0,0-.15.246l-0.84,2.512a0.942,0.942,0,0,0,.13.859,0.963,0.963,0,0,0,.78.4h0a0.927,0.927,0,0,0,.38-0.081l2.44-1.061a0.624,0.624,0,0,0,.23-0.166l7.16-7.8A2,2,0,0,0,1360.35,272.539Zm-9.38,10.2,0.49-1.474,0.05-.045,0.93,0.851-0.04.045Zm8.49-8.315-6.09,6.631-0.93-.851,6.08-6.631a0.614,0.614,0,0,1,.46-0.2,0.641,0.641,0,0,1,.42.162l0.02,0.017A0.62,0.62,0,0,1,1359.46,274.428Zm-0.5,3.919a0.689,0.689,0,0,0-.69.687v5.841a1.757,1.757,0,0,1-1.76,1.749h-8.37a1.757,1.757,0,0,1-1.76-1.749v-8.27a1.756,1.756,0,0,1,1.76-1.749h6.06a0.688,0.688,0,1,0,0-1.376h-6.06a3.137,3.137,0,0,0-3.14,3.125v8.27a3.137,3.137,0,0,0,3.14,3.125h8.37a3.137,3.137,0,0,0,3.14-3.125v-5.841A0.689,0.689,0,0,0,1358.96,278.347Z" transform="translate(-1345 -272)"></path></svg>
                        </a>  
                        <a href="javascript:void(0);" class="common-icon-btn blue-icon-btn" title="" data-tooltip="tooltip" data-original-title="Delete Doctor"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 13 16" fill="#ffffff"><path class="cls-1" d="M1393.75,274H1391v-0.5a1.5,1.5,0,0,0-1.5-1.5h-2a1.5,1.5,0,0,0-1.5,1.5V274h-2.75a1.251,1.251,0,0,0-1.25,1.25V277a0.5,0.5,0,0,0,.5.5h0.27l0.44,9.071A1.487,1.487,0,0,0,1384.7,288h7.6a1.487,1.487,0,0,0,1.49-1.429l0.44-9.071h0.27a0.5,0.5,0,0,0,.5-0.5v-1.75a1.251,1.251,0,0,0-1.25-1.25h0Zm-6.75-.5a0.5,0.5,0,0,1,.5-0.5h2a0.5,0.5,0,0,1,.5.5V274h-3v-0.5Zm-4,1.75a0.249,0.249,0,0,1,.25-0.25h10.5a0.249,0.249,0,0,1,.25.25v1.25h-11v-1.25Zm9.8,11.274a0.51,0.51,0,0,1-.5.476h-7.6a0.51,0.51,0,0,1-.5-0.476l-0.43-9.024h9.46Zm-4.3-.524a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1388.5,286Zm2.5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1391,286Zm-5,0a0.5,0.5,0,0,0,.5-0.5V279a0.5,0.5,0,0,0-1,0v6.5A0.5,0.5,0,0,0,1386,286Z" transform="translate(-1382 -272)"></path></svg></a>
                    </div> 
                )
            }
        }
    ]

 
 

    return(
        <Fragment>
              <div className="dashboard-main-wrapper">
                <Header />
                <Sidebar />
                <div className="dashboard-wrapper">
                    <div className="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h2 class="card-header">Patients</h2>
                                        {/* Verification Reuqest Table */}
                                        <div class="table-responsive table-new">
                                        <Table
                                            data={ patientProfileData }
                                            columns={ patientProfileColumn }
                                        /> 
                                        </div>
                                        {/* End Verification Reuqest Table */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    }

export default PatientProfile;