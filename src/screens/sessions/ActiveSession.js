import React, { Fragment, useState } from 'react';

import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';
// import AddDoctor from './partials/addDoctorModal'; 


const ActiveSession = () => {

    // const [showAddDoctorModal, setShowDoctorModal] = useState(false);
 
    const [doctorProfileData, setProfileData ] =  useState([
        {
            no : 1,
            session_id : 'ff58a66f',
            session_doctor: 'Asif Ali Shaikh',
            session_patient : 'Hafiz Muneer Ahmed',
            session_chief_complaint : 'Hi Dr. Mohsin',
            session_free_interaction : 'No',
            session_packege : 'Pay Per Interaction',
            session_start_at : '2021-08-16 10:47 AM',
            session_amount : 'PKR-300',
            session_detail : '', 
        }, 
    ]);

    // Doctor Profile Columns
    const doctorProfileColumns = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Session ID',
            accessor : 'session_id',
        },
        {
            Header : 'Doctor',
            accessor : 'session_doctor',
        },
        {
            Header : 'Patient',
            accessor : 'session_patient',
        },
        {
            Header : 'Chief Complaint',
            accessor : 'session_chief_complaint',
        },
        {
            Header : 'Free Interaction',
            accessor : 'session_free_interaction',
        },
        {
            Header : 'Packege',
            accessor : 'session_packege',
        },
        {
            Header : 'Started At',
            accessor : 'session_start_at',
        },
        {
            Header : 'Amount',
            accessor : 'session_amount',
        },
        {
            Header : 'Details',
            accessor : 'session_detail',
            Cell: (props) => {
                return (
                    <div class="action_btns">
                        <a href="javascript:void(0);" 
                            id="showpopup"
                            data-toggle="modal"
                            data-target="#view-modal" 
                            data-url="https://admin.doclink.health/doctors/request/show/1220"
                            title="View Details">
                            <img src="https://admin.doclink.health/images/icons/view.svg" alt="Paas `img` Param in `modal_button()` function!" />
                        </a>
                    </div>
                )
            },
        },
 


    ];
    // End Doctor Profile Columns
 



    // let toggleAddDoctorModal = () => {
    //     setShowDoctorModal(!showAddDoctorModal);
    // }

    return (
        <div className="dashboard-main-wrapper">
            <Header />
            <Sidebar />

            {/* <AddDoctor show={showAddDoctorModal} toggle={toggleAddDoctorModal} /> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body coupon-overview-new">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="box-1 p-3">
                                                <h5 class="text-dblue mb-0" style={{fontSize: '11px', lineHheight: '18px'}}>Active Sessions
</h5>
                                                <h1 class="mb-1 coupon-card-main-h">137</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="card-header">
                                    Active Sessions

                                        <button class="btn btn-primary btn-sm btn-icons btn-rounded float-right" title="New Doctor">
                                            <img src="https://admin.doclink.health/images/icons/add.svg" alt="add.svg" class="mr-1" />
                                            </button>
                                    </h2> 
                                    {/* Doctor Profile Tables */}
                                    <div class="table-responsive table-new">
                                    <Table
                                        data={doctorProfileData}
                                        columns={doctorProfileColumns}
                                    /> 
                                    </div>
                                    {/* End Doctor Profile Tables */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}



export default ActiveSession;