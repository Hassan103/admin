import React, { Fragment, useState } from 'react';

import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';
// import AddDoctor from './partials/addDoctorModal'; 


const ChatRequests = () => {

    // const [showAddDoctorModal, setShowDoctorModal] = useState(false);
 
    const [doctorProfileData, setProfileData ] =  useState([
        {
            no : 1,
            chat_request_doctor : 'S. Anwer Hussain',
            chat_request_patient: 'Yamaha',
            chat_request_complaint : 'BABY HAVING COU',
            chat_request_audio : 'N/A',
            chat_request_free_interaction : 'No', 
            chat_request_packeges : 'Pay Per Interaction',
            chat_request_amount : 'PKR 100',
            chat_request_status  : 'Accepted',
            chat_request_sent_at : '24-08-2021 05:42 PM'
        }, 
    ]);

    // Doctor Profile Columns
    const doctorProfileColumns = [
        {
            Header : '#',
            accessor : 'no',
        },
        {
            Header : 'Doctor',
            accessor : 'chat_request_doctor',
        },
        {
            Header : 'Patient',
            accessor : 'chat_request_patient',
        },
        {
            Header : 'Chief Complaint',
            accessor : 'chat_request_complaint',
        },
        {
            Header : 'Audio',
            accessor : 'chat_request_audio',
        },
        {
            Header : 'Free Interaction',
            accessor : 'chat_request_free_interaction',
        },
        {
            Header : 'Packege',
            accessor : 'chat_request_packeges',
        },
        {
            Header : 'Amount',
            accessor : 'chat_request_amount',
        },
        {
            Header : 'Amount',
            accessor : 'session_amount',
        },
        {
            Header : 'Status',
            accessor : 'chat_request_status',
            Cell: (props) => {
                return (
                    <div class="action_btns">
                        <a href="javascript:void(0);" 
                            id="showpopup"
                            data-toggle="modal"
                            data-target="#view-modal" 
                            data-url="https://admin.doclink.health/doctors/request/show/1220"
                            title="View Details">
                            <img src="https://admin.doclink.health/images/icons/view.svg" alt="Paas `img` Param in `modal_button()` function!" />
                        </a>
                    </div>
                )
            },
        },
        {
            Header : 'Sent At',
            accessor : 'chat_request_sent_at'
        }
    ];
    // End Doctor Profile Columns
 



    // let toggleAddDoctorModal = () => {
    //     setShowDoctorModal(!showAddDoctorModal);
    // }

    return (
        <div className="dashboard-main-wrapper">
            <Header />
            <Sidebar />

            {/* <AddDoctor show={showAddDoctorModal} toggle={toggleAddDoctorModal} /> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body coupon-overview-new">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="box-1 p-3">
                                                <h5 class="text-dblue mb-0" style={{fontSize: '11px', lineHheight: '18px'}}>Active Sessions
</h5>
                                                <h1 class="mb-1 coupon-card-main-h">137</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="card-header">
                                    Active Sessions

                                        <button class="btn btn-primary btn-sm btn-icons btn-rounded float-right" title="New Doctor">
                                            <img src="https://admin.doclink.health/images/icons/add.svg" alt="add.svg" class="mr-1" />
                                            </button>
                                    </h2> 
                                    {/* Doctor Profile Tables */}
                                    <div class="table-responsive table-new">
                                    <Table
                                        data={doctorProfileData}
                                        columns={doctorProfileColumns}
                                    /> 
                                    </div>
                                    {/* End Doctor Profile Tables */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}



export default ChatRequests;


 