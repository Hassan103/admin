import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";

import Sidebar from "../../components/sidebar";
import Header from "../../components/header";
import Table from '../../components/partials/react_table';

function Dashboard(props) {

    const [activeTab, setActiveTab] = useState('feedback');
    const [feedbackData, setFeedbackData] = useState([
        {
            no: 1,
            session_date: '23/12/2021',
            session_id: 1234,
            doctor_name: 'Safi Haider',
            session_time: '23:00',
            ratings: 3.5,
        },
        {
            no: 2,
            session_date: '23/12/2021',
            session_id: 1234,
            doctor_name: 'Safi Haider',
            session_time: '23:00',
            ratings: 3.5,
        },
        {
            no: 3,
            session_date: '23/12/2021',
            session_id: 1234,
            doctor_name: 'Safi Haider',
            session_time: '23:00',
            ratings: 3.5,
        },
        {
            no: 4,
            session_date: '23/12/2021',
            session_id: 1234,
            doctor_name: 'Safi Haider',
            session_time: '23:00',
            ratings: 3.5,
        },
    ]);
    const [problemsData, setProblemsData] = useState([]);

    const feedbackColumns = [
        {
            Header: '#',
            accessor: 'no', // accessor is the "key" in the data
        },
        {
            Header: 'Session Date',
            accessor: 'session_date',
        },
        {
            Header: 'Session ID',
            accessor: 'session_id',
        },
        {
            Header: 'Doctor Name',
            accessor: 'doctor_name',
        },
        {
            Header: 'Session Time',
            accessor: 'session_time',
        },
        {
            Header: 'Ratings',
            accessor: 'ratings',
            Cell: (props) => {
                return (
                    <></>
                )
            },
        },
        {
            Header: 'Actions',
            accessor: 'actions',
            Cell: (props) => {
                return (
                    <></>
                )
            },
        },
    ]

    const problemsColumns = [
        {
            Header: '#',
            accessor: 'no', // accessor is the "key" in the data
        },
        {
            Header: 'User Name',
            accessor: 'user_name',
        },
        {
            Header: 'Title',
            accessor: 'title',
        },
        {
            Header: 'Category',
            accessor: 'category',
        },
        {
            Header: 'Subject',
            accessor: 'subject',
        },
        {
            Header: 'Timestamp',
            accessor: 'timestamp',
        },
        {
            Header: 'Actions',
            accessor: 'actions',
            Cell: (props) => {
                return (
                    <></>
                )
            },
        },
    ]

    return (
        <div className="dashboard-main-wrapper">
            <Header />
            <Sidebar />

            <div className="dashboard-wrapper">
                <div className="container-fluid">
                    <div className="dashboard-portal">
                        <div className="table-main">
                            <Nav tabs>
                                <NavItem>
                                    <NavLink active={activeTab === 'feedback'} onClick={() => setActiveTab('feedback')}>Session Feedbacks</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink active={activeTab === 'problems'} onClick={() => setActiveTab('problems')}>Problems</NavLink>
                                </NavItem>
                            </Nav>

                            <TabContent activeTab={activeTab}>
                                <TabPane tabId='feedback'>
                                    <Table
                                        data={feedbackData}
                                        columns={feedbackColumns}
                                    />
                                    <div className="text-right">
                                        <Link to="feedback" className="btn btn-primary btn-xs btn-rounded text-white nounderline">View More</Link>
                                    </div>
                                </TabPane>
                                <TabPane tabId='problems'>
                                    <Table
                                        data={problemsData}
                                        columns={problemsColumns}
                                    />
                                    <div className="text-right">
                                        <Link to="reports" className="btn btn-primary btn-xs btn-rounded text-white nounderline">View More</Link>
                                    </div>
                                </TabPane>
                            </TabContent>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;