import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import validator from 'validator';

import { signIn, checkSignIn } from '../shared/services/user';
import Logo from '../assets/images/logo.png'

const Login = () => {

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState({
        emailError: null,
        passwordError: null,
    });

    useEffect(() => {
        if (checkSignIn()) {
            history.push("/dashboard");
        }
    }, []);

    let handleEmailChange = (e) => {
        setEmail(e.target.value);
        if (validator.isEmail(e.target.value)) {
            errors.emailError = null;
        }
        else {
            errors.emailError = 'Invalid Email.';
        }
    }

    let handlePasswordChange = (e) => {
        setPassword(e.target.value);
    }

    let handleSubmit = async (e) => {
        e.preventDefault();

        if (!errors.emailError) {
            let data = {
                email,
                password
            }

            let res = await signIn(data);

            if (!res.error) {
                if (res.message === 'Login successfully.') {
                    history.push("/dashboard");
                }
            }
        }
    }

    return (
        <>
            <div className="bgcover">
                <div className="splash-container">
                    <div className="card ">
                        <div className="card-header text-center">
                            <img class="logo-img" src={Logo} alt="logo" />
                            <div className="splash-description">Please enter your user information.</div>
                        </div>
                        <p className="alert alert-info">
                            The Credidential do not match
                        </p>
                        <div className="card-body">
                            <form className="form-horizontal" method="POST" onSubmit={handleSubmit}>
                                <input type="hidden" name="_token" value="HjBbIic7FZlBi4xXGNZL1QI2hgFWrXPQKP8Y3beh" />
                                <div className="form-group">
                                    <input id="email" type="email" value={email} placeholder="Email" className="form-control form-control-lg" name="email" autofocus="" autocomplete="off" onChange={handleEmailChange} />
                                </div>
                                <div className="form-group">
                                    <input id="password" type="password" value={password} className="form-control form-control-lg" name="password" placeholder="Password" onChange={handlePasswordChange} />
                                </div>
                                <div className="form-group">
                                    <label className="custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" name="remember" />
                                        <span className="custom-control-label">Remember Me</span>
                                    </label>
                                </div>
                                <button type="submit" className="btn btn-primary btn-lg btn-block">Login</button>

                            </form>
                        </div>
                        <div className="card-footer bg-white p-0 text-center ">
                            <div className="card-footer-item card-footer-item-bordered">
                                <a href="http://45.249.8.93/doclink-admin/public/password/reset" className="footer-link">Forgot Password</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {/* <div className="container signup_form my-5">
                <form action="" onSubmit={handleSubmit} className="form justify-content-center p-0">
                    <div className="form-group col-12 m-0 mb-4 p-0">
                        <h2 className="text-center">Log In</h2>
                    </div>
                    <div className="p-3">
                        <div className="form-group col-12">
                            <input type="email" value={email} className="form-control my_input" placeholder="Email Address" required onChange={handleEmailChange} />
                            {errors.emailError ? <div className="error">{errors.emailError}</div> : null}
                        </div>
                        <div className="form-group col-12">
                            <input type="password" value={password} className="form-control my_input" placeholder="Password" required onChange={handlePasswordChange} />
                            {errors.passwordError ? <div className="error">{errors.passwordError}</div> : null}
                        </div>
                        <div className="form-group col-12 m-0 mt-5">
                            <a href="">Forgot your password?</a>
                        </div>
                        <div className="form-group col-12 mt-3">
                            <input type="submit" className="form-control btn" value="Log In" />
                        </div>
                        <hr className="clearfix col-12 mb-3 p-0" />
                    </div>
                </form>
            </div> */}
        </>
    );
};

export default Login;