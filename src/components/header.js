import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

// Avatar

import Avatar from '../assets/images/dummy.png'

function Header(props) {

    const [profileDropdown, setProfileDropdown] = useState(false);

    const toggleProfileDropdown = () => setProfileDropdown(prevState => !prevState);

    return (
        <div className="dashboard-header">
            <div className="top-nav d-flex fixed-top flex-wrap">
                <div className="header-search position-relative">
                    <input type="text" name="query" id="query" size="30" maxlength="30" placeholder="Search" readonly="" />
                    <span className="icon-search">
                        <svg width="18px" height="18px" viewBox="0 0 16 16" className="bi bi-search" fill="#8e9194" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                        </svg>
                        <svg width="18px" height="18px" viewBox="0 0 16 16" className="bi bi-x-circle d-none" fill="currentColor" xmlns="http://www.w3.org/2000/svg" onclick="window.location.reload()">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
                </div>
                <div className="d-flex align-items-center">
                    <Dropdown isOpen={profileDropdown} toggle={toggleProfileDropdown}>
                        <DropdownToggle className="d-flex align-items-center">
                            <img src={Avatar} alt="dummy.png" className="user-avatar-md rounded-circle" />
                            <span className="svg-arrow d-inline-block">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 30 30"><path d="M 24.990234 8.9863281 A 1.0001 1.0001 0 0 0 24.292969 9.2929688 L 15 18.585938 L 5.7070312 9.2929688 A 1.0001 1.0001 0 0 0 4.9902344 8.9902344 A 1.0001 1.0001 0 0 0 4.2929688 10.707031 L 14.292969 20.707031 A 1.0001 1.0001 0 0 0 15.707031 20.707031 L 25.707031 10.707031 A 1.0001 1.0001 0 0 0 24.990234 8.9863281 z"></path></svg>
                            </span>
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem>
                                <i className="fa fa-user mr-2"></i>Edit Profile
                            </DropdownItem>
                            <DropdownItem>
                                <i className="fa fa-cog mr-2"></i>Change Password
                            </DropdownItem>
                            <DropdownItem>
                                <i className="fa fa-power-off mr-2"></i>Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </Dropdown>

                    <div className="user-info">
                        <h5 className="mb-0 nav-user-name">Admin </h5>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;