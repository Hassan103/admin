import React, { useState } from 'react';

import { Link } from 'react-router-dom';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText
} from 'reactstrap';

// Icons

import Logo from '../assets/images/logo3.png'
import Dashboard from '../assets/images/icons/sidebar-icons/dashboard.svg';

import Doctors from '../assets/images/icons/sidebar-icons/doctors.svg';
import User from '../assets/images/icons/sidebar-icons/inner/user.svg';
import Connections from '../assets/images/icons/sidebar-icons/inner/doctor-connections.svg';
import Requests from '../assets/images/icons/sidebar-icons/inner/verification.svg';
import Invitations from '../assets/images/icons/sidebar-icons/inner/invited.svg';
import SignUps from '../assets/images/icons/sidebar-icons/inner/signups.svg';
import Packages from '../assets/images/icons/sidebar-icons/inner/packages.svg';
import Specializations from '../assets/images/icons/sidebar-icons/inner/specializations.svg';
import Clinics from '../assets/images/icons/sidebar-icons/inner/clinics.svg';
import Sales from '../assets/images/icons/sidebar-icons/inner/sales_referrals.svg';
import Rejected from '../assets/images/icons/sidebar-icons/inner/doctor_rejected.svg';

import Patients from '../assets/images/icons/sidebar-icons/patients.svg';

import Sessions from '../assets/images/icons/sidebar-icons/sessions.svg';
import ActiveSessions from '../assets/images/icons/sidebar-icons/inner/active_sessions.svg';
import ChatRequests from '../assets/images/icons/sidebar-icons/inner/chat_requests.svg';

import Finance from '../assets/images/icons/sidebar-icons/finance.svg';
import TopUps from '../assets/images/icons/sidebar-icons/inner/topups.svg';
import Transactions from '../assets/images/icons/sidebar-icons/inner/transactions.svg';
import Settlements from '../assets/images/icons/sidebar-icons/inner/settlements.svg';

import Card from '../assets/images/icons/sidebar-icons/card.svg';
import Pool from '../assets/images/icons/sidebar-icons/inner/card_pool.svg';
import Assigned from '../assets/images/icons/sidebar-icons/inner/assigned_cards.svg';
import Recovery from '../assets/images/icons/sidebar-icons/inner/card_recovery.svg';

import Feedback from '../assets/images/icons/sidebar-icons/user_feedback.svg';
import ReportProblems from '../assets/images/icons/sidebar-icons/inner/reported_problems.svg';
import SessionFeedbacks from '../assets/images/icons/sidebar-icons/inner/feedback.svg';

import Settings from '../assets/images/icons/sidebar-icons/settings.svg';
import RoleManagement from '../assets/images/icons/sidebar-icons/inner/role_management.svg';
import Logs from '../assets/images/icons/sidebar-icons/inner/activity_log.svg';
import About from '../assets/images/icons/sidebar-icons/inner/about_us.svg';
import PrivacyPolicy from '../assets/images/icons/sidebar-icons/inner/privacy_policy.svg';
import Terms from '../assets/images/icons/sidebar-icons/inner/terms_of_use.svg';

import Stories from '../assets/images/icons/sidebar-icons/communication.svg';

let navigations = [
    {
        name: 'Dashboard',
        permission: '',
        active_class: '/dashboard',
        route: '/dashboard',
        icon: Dashboard,
    },
    {
        name: 'Doctors',
        dropdown: true,
        permission: '',
        active_class: ['/doctors', '/verification-request', '/invitations', '/connections', '/signups', '/packages', '/specializations', '/clinics', '/sales/referral', '/rejected'],
        icon: Doctors,
        subNavs: [
            {
                name: 'Profiles',
                permission: '',
                active_class: '/doctors',
                route: '/doctors',
                icon: User,
            },
            {
                name: 'Connections',
                permission: '',
                active_class: '/doctors/connections',
                route: '/doctors/connections',
                icon: Connections,
            },
            {
                name: 'Verification Requests',
                permission: '',
                active_class: '/doctors/verification-request',
                route: '/doctors/verification-request',
                icon: Requests,
            },
            {
                name: 'Invited',
                permission: '',
                active_class: '/doctors/invitations',
                route: '/doctors/invitations',
                icon: Invitations,
            },
            {
                name: 'Sign ups',
                permission: '',
                active_class: '/doctors/signups',
                route: '/doctors/signups',
                icon: SignUps,
            },
            {
                name: 'Packages',
                permission: '',
                active_class: '/doctors/packages',
                route: '/doctors/packages',
                icon: Packages,
            },
            {
                name: 'Specializations',
                permission: '',
                active_class: '/doctors/specializations',
                route: '/doctors/specializations',
                icon: Specializations,
            },
            {
                name: 'Clinics',
                permission: '',
                active_class: '/doctors/clinics',
                route: '/doctors/clinics',
                icon: Clinics,
            },
            {
                name: 'Sales Referrals',
                permission: '',
                active_class: '/doctors/sales/referral',
                route: '/doctors/sales/referral',
                icon: Sales,
            },
            {
                name: 'Rejected Requests',
                permission: '',
                active_class: '/doctors/rejected',
                route: '/doctors/rejected',
                icon: Rejected,
            },
            
        ]
    },
    {
        name: 'Patients',
        dropdown: true,
        permission: '',
        active_class: ['/patients', '/wallet_top_up_history_admin'],
        icon: Patients,
        subNavs: [
            {
                name: 'Profiles',
                permission: '',
                active_class: '/patients',
                route: '/patients',
                icon: User,
            },
        ]
    },
    {
        name: 'Sessions',
        dropdown: true,
        permission: '',
        active_class: ['/session/active', 'session/chat/requests'],
        icon: Sessions,
        subNavs: [
            {
                name: 'Active Sessions',
                permission: '',
                active_class: '/session/active',
                route: '/session/active',
                icon: ActiveSessions,
            },
            {
                name: 'Chat Requests',
                permission: '',
                active_class: 'session/chat/requests',
                route: '/session/chat/requests',
                icon: ChatRequests,
            },
        ]
    },
    {
        name: 'Finance',
        dropdown: true,
        permission: '',
        active_class: ['/finance/transferred', '/finance/transactions', '/finance/settlements'],
        icon: Finance,
        subNavs: [
            {
                name: 'Top-ups',
                permission: '',
                active_class: '/finance/transferred',
                route: '/finance/transferred',
                icon: TopUps,
            },
            {
                name: 'Transactions',
                permission: '',
                active_class: '/finance/transactions',
                route: '/finance/transactions',
                icon: Transactions,
            },
            {
                name: 'Settlements',
                permission: '',
                active_class: '/finance/settlements',
                route: '/finance/settlements',
                icon: Settlements,
            },
        ]
    },
    {
        name: 'Prepaid Card',
        dropdown: true,
        permission: '',
        active_class: ['/prepaid/coupons', '/prepaid/coupons/assigned', '/prepaid/coupons/recovery'],
        icon: Card,
        subNavs: [
            {
                name: 'Pool',
                permission: '',
                active_class: '/prepaid/coupons',
                route: '/prepaid/coupons',
                icon: Pool,
            },
            {
                name: 'Assigned',
                permission: '',
                active_class: '/prepaid/coupons/assigned',
                route: '/prepaid/coupons/assigned',
                icon: Assigned,
            },
            {
                name: 'Recovery',
                permission: '',
                active_class: '/prepaid/coupons/recovery',
                route: '/prepaid/coupons/recovery',
                icon: Recovery,
            },
        ]
    },
    {
        name: 'User Feedback',
        dropdown: true,
        permission: '',
        active_class: ['/feedback/reports', '/feedback/session'],
        icon: Feedback,
        subNavs: [
            {
                name: 'Report Problems',
                permission: '',
                active_class: '/feedback/reports',
                route: '/feedback/reports',
                icon: ReportProblems,
            },
            {
                name: 'Session Feedbacks',
                permission: '',
                active_class: '/feedback/session',
                route: '/feedback/session',
                icon: SessionFeedbacks,
            },
        ]
    },
    {
        name: 'Stories',
        permission: '',
        active_class: '/stories',
        route: '/stories',
        icon: Stories,
    },
    {
        name: 'Settings',
        dropdown: true,
        permission: '',
        active_class: ['/settings/roles', '/settings/logs', '/settings/about-us', 'settings/privacy', 'settings/terms'],
        icon: Settings,
        subNavs: [
            {
                name: 'Role Management',
                permission: '',
                active_class: '/settings/roles',
                route: '/settings/roles',
                icon: RoleManagement,
            },
            {
                name: 'Activity Log',
                permission: '',
                active_class: '/settings/logs',
                route: '/settings/logs',
                icon: Logs,
            },
            {
                name: 'Privacy Policy',
                permission: '',
                active_class: '/settings/privacy',
                route: '/settings/privacy',
                icon: PrivacyPolicy,
            },
            {
                name: 'Terms of Use',
                permission: '',
                active_class: '/settings/terms',
                route: '/settings/terms',
                icon: Terms,
            },
            {
                name: 'About Us',
                permission: '',
                active_class: '/settings/about-us',
                route: '/settings/about-us',
                icon: About,
            },
        ]
    },
];

const Sidebar = (props) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div className="nav-left-sidebar sidebar-dark">
            <div className="slimScrollDiv">
                <div className="menu-list">
                    <Navbar expand="md">
                        <NavbarBrand href="/" className="d-xl-none d-lg-none">
                            <img src={Logo} width="180" />
                        </NavbarBrand>
                        <NavbarToggler onClick={toggle} />
                        <Collapse isOpen={isOpen} navbar>
                            <Nav className="mr-auto flex-column" navbar>
                                <Link className="d-none d-lg-block logo-inner">
                                    <img src={Logo} width="180" />
                                </Link>
                                <div className="collapse show mt-2" id="collapse-my-portal">
                                    {
                                        navigations.map(nav => (
                                            <>
                                                {nav.dropdown ?
                                                    <UncontrolledDropdown nav inNavbar className={nav.active_class.includes(window.location.pathname) ? 'active' : null}>
                                                        <DropdownToggle nav>
                                                            <img class="icon" src={nav.icon} alt="" />
                                                            <span class="text">{nav.name}</span>
                                                            <div class="arrow close-arrow">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="12">
                                                                    <image data-name="arrow-right copy 10" width="7" height="12" href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAMCAQAAAAEnG+bAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfkBxUOACysoMlgAAAAbUlEQVQI10XOoQpCUQCD4XHFYLgmm0bBdKu+pdEnUAyCIFg1m60abRrl1s9wOLiln8G2WDoaSXFccTKsOPPEVlMw5l7YVIzOG+uKsfLFvknRJ32SRcmmHjgYREzca3e0bv/l2OFSf0XnbFxP/gD1/mfgNPXxXgAAAABJRU5ErkJggg=="></image>
                                                                </svg>
                                                            </div>
                                                            <div class="arrow close-active-arrow">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="12">
                                                                    <image data-name="arrow-right copy 10" width="7" height="12" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAMCAYAAACulacQAAAAyElEQVQYlV3Qr0pEQRgF8N+umkQMjkmjYF9QX0GLYFIUTLdYd4qPsGVMGwQHTILBZNGiT6JN222CBlmujNy9rH7p8B3OH05vZXi1jXOc1Kn6MnN9jHCA2xDzwn/yFK/Yx3WIud+RdaresIv3Yo3xrFKdqhfsFYizEPNF+feapukyQsw7eMJi6dD5t1eUny3e7MgQ8xoesYo7bP2SIeaAB2zgBod1qiZz44/1pVYxwH07xmTa9hKlyDOO6lR9T6Pm24WWcfxnPvwAiFg1mRpOlXgAAAAASUVORK5CYII="></image>
                                                                </svg>
                                                            </div>
                                                            <div class="arrow open-arrow">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="12" height="7">
                                                                    <image data-name="arrow-right copy 3" width="12" height="7" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAHCAYAAAA8sqwkAAAAuUlEQVQYlW3Pry+GURTA8c+LG7yqRqPajW/WJElB0gRJ8PgP7CZRtNlsVNNspiueaPMfiOLdPHbssvc1p5ztnO/3/BgtnLxs4BSHteQP/0Tq+lVcBTeHM+zhIXX98l88df06nrD5IwT8iEnk1PUrU3BsD3gNd9gfDcMQjUXcYBtv2EJsu2/5Gge15PottGkJlzEF7xhjCRc4qiV/BvcrNClOvMVOK53Xko+nf5oRmjSPZ7zWkndnmvgCddQ5RCTgxm8AAAAASUVORK5CYII="></image>
                                                                </svg>
                                                            </div>
                                                        </DropdownToggle>
                                                        <DropdownMenu right className={nav.active_class.includes(window.location.pathname) ? 'active' : null}>
                                                            {nav.subNavs.map(sub => (
                                                                
                                                                <Link to={sub.route}>
                                                                    { console.log(sub.route, "sub.route") }
                                                                    <DropdownItem className={window.location.pathname === sub.active_class ? 'active' : null}>
                                                                        <img class="icon" src={sub.icon} alt="" />
                                                                        {sub.name}
                                                                    </DropdownItem>
                                                                </Link>
                                                            ))}
                                                        </DropdownMenu>
                                                    </UncontrolledDropdown>
                                                    :
                                                    <NavItem className={window.location.pathname === nav.active_class ? 'active' : null}>
                                                        <NavLink href={nav.route} className={window.location.pathname === nav.active_class ? 'active' : null}>
                                                            <img className="icon single" src={nav.icon} alt="" />
                                                            <span className="text">{nav.name}</span>
                                                        </NavLink>
                                                    </NavItem>
                                                }
                                            </>
                                        ))
                                    }
                                </div>
                            </Nav>
                        </Collapse>
                    </Navbar>
                    <div className="show-version">v2.0.4</div>
                </div>
                <div className="slimScrollBar"></div>
                <div className="slimScrollRail"></div>
            </div>
        </div>
    );
}

export default Sidebar;