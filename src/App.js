import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Components
import Login from './components/login';
// Screens
import Dashboard from './screens/dashboard/dashboard';
import DoctorProfile from './screens/doctors/DoctorPofile';
import VerificationRequest from './screens/doctors/VerificationRequest';
import DoctorInvite from './screens/doctors/DoctorInvite'; 
import DoctorPackeges from './screens/doctors/DoctorPackeges';
import DoctorSpecialization from './screens/doctors/DoctorSpecialization';
import DoctorClinics from './screens/doctors/DoctorClinics';
import DoctorSalesReferal from './screens/doctors/DoctorSalesReferral';
import DoctorRejectRequest from './screens/doctors/DoctorRejectRequest';
import DoctorSignup from './screens/doctors/DoctorSignup';
import DoctorsConnections from './screens/doctors/DoctorsConnections';
import PatientProfile from './screens/patients/PatientProfile';
import ActiveSession from './screens/sessions/ActiveSession';
import ChatRequests from './screens/sessions/ChatRequests';
import TopUps from './screens/finance/TopUps';
import Transactions from './screens/finance/Transactions';
import Settlements from './screens/finance/Settlements';
import Assigned from './screens/prepaid-card/Assigned';
import Pools from './screens/prepaid-card/Pools';
import Recovery from './screens/prepaid-card/Recovery';
import ReportProblems from './screens/user-feedback/ReportProblems';
import SessionFeedback from './screens/user-feedback/SessionFeedback';
import StoriesIndex from './screens/stories/StoriesIndex';
import RoleManagment from './screens/settings/RoleManagment';
import ActivityLog from './screens/settings/ActivityLog';
import PrivacyPolicy from './screens/settings/PrivacyPolicy';
import TermsConditions from './screens/settings/TermsConditions';
import AboutUs from './screens/settings/AboutUs';
function App() {
  return (
    <BrowserRouter>
      <Switch>
        {/************************************* Dashboard Routes ***************************************/}
        <Route exact path="/dashboard" name="Dashboard" component={Dashboard} />
        {/************************************* End Dashboard Routes ***************************************/}

        {/************************************* Login Routes ***************************************/}
        <Route exact path="/(|login)" name="Login" component={Login} />
        {/************************************* End Login Routes ***************************************/}

        {/************************************* Doctor Routes ***************************************/}
        <Route exact path="/doctors" name="doctors" component={ DoctorProfile } />
        <Route exact path="/doctors/verification-request" name="doctor-request" component={ VerificationRequest } />
        <Route exact path="/doctors/invitations" name="doctor-invitation" component={ DoctorInvite } /> 
        <Route exact path="/doctors/packages" name="doctor-packegs" component={ DoctorPackeges } /> 
        <Route exact path="/doctors/specializations" name="doctor-specialization" component={ DoctorSpecialization } /> 
        <Route exact path="/doctors/clinics" name="doctor-clinics" component={ DoctorClinics } /> 
        <Route exact path="/doctors/sales/referral" name="doctors-sales-referral" component={ DoctorSalesReferal } /> 
        <Route exact path="/doctors/rejected" name="doctors-rejected" component={ DoctorRejectRequest } />
        <Route exact path="/doctors/signups" name="doctors-signup" component={ DoctorSignup } />
        <Route exact path="/doctors/connections" name="doctors-connections" component={ DoctorsConnections } />
        {/************************************* End Doctor Routes ***************************************/}

        {/************************************* Patient Routes ***************************************/}
        <Route exact path="/patients" name="patient" component={ PatientProfile } />
        {/************************************* End Patient Routes ***************************************/}

        {/************************************* Session Routes ***************************************/}
        <Route exact path="/session/active" name="session-active" component={ ActiveSession } />
        <Route exact path="/session/chat/requests" name="session-chat" component={ ChatRequests } />
        {/************************************* End Session Routes ***************************************/}

        {/************************************* Finance Routes ***************************************/}
        <Route exact path="/finance/transferred" name="finance-topups" component={ TopUps } />
        <Route exact path="/finance/transactions" name="finance-transactions" component={ Transactions } />
        <Route exact path="/finance/settlements" name="finance-settlements" component={ Settlements } />
        {/************************************* End Finance Routes ***************************************/}



        {/************************************* Prepaid Routes ***************************************/}
        <Route exact path="/prepaid/coupons/assigned" name="prepaid-assign"  component={ Assigned } />
        <Route exact path="/prepaid/coupons"  name="prepaid-pools" component={ Pools } />
        <Route exact path="/prepaid/coupons/recovery"  name="prepaid-recovery" component={ Recovery } />
        {/************************************* End Prepaid Routes ***************************************/}


        {/************************************* User Feedback Routes ***************************************/}
        <Route exact path="/feedback/reports"  name="feedback-reports" component={ ReportProblems } />
        <Route exact path="/feedback/session"  name="feedback-session" component={ SessionFeedback } />
        {/************************************* End User Feedback Routes ***************************************/}

 
        
        {/************************************* Stories Routes ***************************************/}
        <Route exact path="/stories"  name="stories" component={ StoriesIndex } />
        {/************************************* End Stories Routes ***************************************/}

        {/************************************* Settings Routes ***************************************/}

        <Route exact path="/settings/roles"  name="setting-roles" component={ RoleManagment } />
        <Route exact path="/settings/logs"  name="setting-logs" component={ ActivityLog } />
        <Route exact path="/settings/privacy"  name="setting-privacy-policy" component={ PrivacyPolicy } />
        <Route exact path="/settings/terms"  name="setting-terms" component={ TermsConditions } />
        <Route exact path="/settings/about-us"  name="setting-about-us" component={ AboutUs } />
        
        {/************************************* End Settings Routes ***************************************/}




        

        {/* End Doctor Routes */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
